init = () => {
  console.log('initialised index js!');
  animateProgressBar()
  checkInitialService()
}

const formEl = document.getElementById('js-hook-for-form')
const step1dom = document.getElementById('js-hook-for-contract')
const step2dom = document.getElementById('js-hook-for-numbers')
const step3dom = document.getElementById('js-hook-for-name-address')
const step4dom = document.getElementById('js-hook-for-termination-text')

const modal = document.getElementById('modal-popup')
const modalClose = document.getElementById('popup-modal-close')
const bodyDrop = document.getElementById('body-drop')

const modalTitle = document.getElementById('js-hook-for-modal-title')
const modalText = document.getElementById('js-hook-for-modal-text')
const modalImg = document.getElementById('js-hook-for-modal-img')
const modalCta = document.getElementById('js-hook-for-modal-cta')

// in normal situation this should be set via service endpoint response 
const lastStepText = `hiermit kündige ich meinen Mobiles Internet-Vertrag fristgerecht, hilfsweise zum nächstmöglichen Zeitpunkt. Bitte senden Sie mir eine schriftliche Bestätigung der Kündigung unter Angabe des Beendigungszeitpunktes zu.

Sofern Ihnen für den betreffenden Vertrag eine Einzugsermächtigung vorliegt, widerrufe ich diese zum Ablauf des Vertrages.

Jegliche Form der Kontaktaufnahme Ihrerseits zum Zweck der Rückwerbung ist nicht erwünscht und ich bitte freundlich darum, davon abzusehen.`

let state = {
  percent: 0,
  service: 0,
  submitCallback: null,
  modalCallback: null
}

animateProgressBar = (percent) => {
  const progressBarElem = document.getElementById('js-hook-for-animated-progress')
  const progressBarDom = document.getElementById('js-hook-for-animated-counter')
  const percentage = !!percent ? percent : 20
  const FPS = Math.ceil(1000 / 24);
  
  const domText = (percentToDom) => {
    const percentString = percentToDom + '%'
    progressBarDom.textContent = percentString
    progressBarElem.style.width = percentString
  }
  const percentInterval = setInterval(() => {
    if (percentage > state.percent++) {
      domText(state.percent)
    } else {
      console.log(state.percent)
      clearInterval(percentInterval)
    }
  }, FPS)
}

checkInitialService = () => {
  const formInputs = document.querySelectorAll('.js-hook-for-multiple-form-radio-service')
  
  const selectedService = state.service
  if (selectedService) {
    const selectedInput = formInputs.querySelector(`input[value="${selectedService}]`)
    selectedInput.checked = 'checked';
  }
  const radioChangeCb = (event) => {
    event.preventDefault();
    const inputElem = event.target;
    const inputVal = inputElem.value.trim()
    state.service = inputVal
    renderModalKundennummer()
  }

  const formSubmitCb = (event) => {
    event.preventDefault()
    // state.submitCallback = formSubmitCb
    if (state.service) {
      stateResolver()
    }
  }
  
  formInputs.forEach((formInput) => {
    formInput.addEventListener('change', radioChangeCb)
  })

  formEl.removeEventListener('submit', state.submitCallback)
  formEl.addEventListener('submit', formSubmitCb)
  state.submitCallback = formSubmitCb

}

stateResolver = () => {
  const percentage = state.percent

  switch (percentage) {
    case 21:
      step1dom.classList.add("dn")
      step2dom.classList.remove("dn")
      animateProgressBar(40)
      checkFormInputs(step2dom)
      break;
    case 41:
      step2dom.classList.add("dn")
      step3dom.classList.remove("dn")
      animateProgressBar(60)
      checkFormInputs(step3dom)
      break;
    case 61:
      step3dom.classList.add("dn")
      step4dom.classList.remove("dn")
      animateProgressBar(80)
      checkLastStep()
      break;
    case 81:
      animateProgressBar(100)
      showSuccessModal()
      break;
  }
}


checkFormInputs = (parentDom) => {
  const formInputs = parentDom.querySelectorAll('.js-hook-for-multiple-form-input')
  
  const formChangeCb = (event) => {
    const inputElem = event.target;
    const inputVal = inputElem.value.trim()
    const fieldElem = inputElem.parentNode;
    fieldElem.classList.remove('pristine')
    fieldElem.classList.remove('error')
    if (!!inputElem.value) {
      fieldElem.classList.add('pristine')
    }
    if (inputElem.required && !inputVal) {
      fieldElem.classList.add('error')
    }
  }

  const checkIfFormIsValid = (inputElem) => {
    const inputVal = inputElem.value.trim()
    return !inputElem.required || inputVal
  }

  const formSubmitCb = (event) => {
    event.preventDefault();
    state.submitCallback = formSubmitCb
    const isValid = [...formInputs].every(checkIfFormIsValid)
    if (!isValid) {
      alert('should display modal popup with error message :)')
      console.log('form validation failed!');
      return;
    }
    console.log('succesfully progressing to next step!');
    stateResolver()
  }

  formInputs.forEach((formInput) => {
    if (formInput.classList.contains('js-hook-for-required-field')) {
      formInput.setAttribute("required", "required")
    }
    formInput.addEventListener('change', formChangeCb)
  })

  formEl.removeEventListener('submit', state.submitCallback)
  formEl.addEventListener('submit', formSubmitCb)
}

const checkLastStep = () => {
  const textParent = step4dom.querySelector('#js-hook-for-terminationText')
  const textArea = step4dom.querySelector('#terminationText')
  const textSpan = textParent.querySelector('span')
  const textAnchor = textParent.querySelector('a')

  const textAreaChangeCb = (event) => {
    const inputVal = !event ? textArea.value : event.target.value.trim()
    textSpan.innerText = inputVal
  }

  const formSubmitCb = (event) => {
    event.preventDefault();
    state.submitCallback = formSubmitCb
    console.log('succesfully completed the funnel!');
    stateResolver()
  }

  const toggleForms = () => {
    const textAreaHidden = textArea.parentNode.classList.contains("dn")
    if (!textAreaHidden) {
      textArea.parentNode.classList.add("dn")
      textSpan.classList.remove("dn")
    } else {
      textArea.parentNode.classList.remove("dn")
      textSpan.classList.add("dn")
    }
    textAnchor.innerText = !textAreaHidden ? 'Bearbeiten' : 'Komplett'
    textAreaChangeCb()
  }

  textAnchor.addEventListener('click', toggleForms)

  textArea.addEventListener('change', textAreaChangeCb)

  textArea.value = textSpan.innerText = lastStepText;
  
  formEl.removeEventListener('submit', state.submitCallback)
  formEl.addEventListener('submit', formSubmitCb)
} 

const showSuccessModal = () => {
  renderModal({
    title: 'Operation erfolgreich',
    text: 'Ihre Dokumente wurden erfolgreich erstellt. Sendebereit.',
    cta: 'Weiter'
  })
}

renderModal = (modalObject) => {
  modalImg.classList.add('dn')
  modalTitle.innerText = modalObject.title
  modalText.innerText = modalObject.text
  modalCta.innerText = modalObject.cta
  modalCta.removeEventListener('click', state.modalCallback)
  modalCta.addEventListener('click', ()=> {
    hideModal()
    alert('SUCCESS! should navigate to the shipping page now!')
  })
  showModal()
}

renderModalKundennummer = () => {
  const enumOfService = [
    {id: 79, name: 'Anti-Virus Software'},
    {id: 18, name: 'Internet'},
    {id: 88, name: 'Mobiles Internet'},
    {id: 14, name: 'Mobilfunk'},
    {id: 37, name: 'Webservices'}
  ]
  const selectedService = enumOfService.filter(e => e.id == +state.service)
  const selectedServiceName = selectedService[0].name
  
  modalTitle.innerText = selectedServiceName
  modalText.innerText = 'Bitte stellen Sie sicher, dass Ihre erforderlichen Dokumente bereit sind.'
  modalImg.classList.remove('dn')

  modalCta.innerText = 'Meine Dokumente sind fertig'
  if (!state.modalCallback) {
    const modalCb = () => {
      hideModal()
      proceed()
    }
    state.modalCallback = modalCb
    modalCta.removeEventListener('click', state.modalCallback)
    modalCta.addEventListener('click', modalCb)
  }
  
  showModal()
}

const proceed = () => setTimeout(() => {
  stateResolver()
  console.log(proceed)
}, 501)

const showModal = () => {
  modal.classList.add('is-visible')
  bodyDrop.classList.add('d-visible')
}

const hideModal = () => {
  modal.classList.remove('is-visible')
  bodyDrop.classList.remove('d-visible')
  if (!proceed) clearTimeout(proceed)
}

window.addEventListener('load', init)